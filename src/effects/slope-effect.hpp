//Copyright (c) 2022, MIT License, Daniel Williams

#pragma once


namespace Effects {

/// \warning the units on this are still wrong
template< typename layer_t >
void apply_slope_effect( float slope_x, float slope_y, layer_t& target_layer_ ){
    printf(">>> Apply::Slope-Effect: { %5.2f, %5.2f }\n", slope_x, slope_y );

    for( size_t xi = 0; xi < target_layer_.size_x(); ++xi ){
        for( size_t yj = 0; yj < target_layer_.size_y(); ++yj ){
            uint8_t& height_at = target_layer_.height( xi, yj );
            const int slope_delta = (xi*slope_x) + (yj*slope_y);
            const int new_height = height_at + slope_delta;

            // clamp new-height to range: [0,255]
            if( new_height <= 0 ){
                height_at = 0;
            }else if( UINT8_MAX < new_height ){
                height_at = 0xFF;
            }else{
                height_at = static_cast<uint8_t>(new_height);
            }
        }
    }
};

}   // namespace Effect
