//Copyright (c) 2022, MIT License, Daniel Williams

#pragma once

#include <cstdint>

namespace Effects {

template< typename layer_t >
void apply_fill_effect( uint8_t threshold_height, layer_t& target_layer ){
    for( size_t i=0; i < target_layer.size(); ++i ){
        uint8_t& at = target_layer.height(i);
        if( at < threshold_height ){
            at = threshold_height;
        }
    }
}


}   // namespace Effect

