//Copyright (c) 2022, MIT License, Daniel Williams

#pragma once

#include <cstdint>

namespace Effects {

template< typename layer_t >
void apply_step_effect( uint8_t start_height, uint8_t level_height, layer_t& target_layer ){
    for( size_t i=0; i < target_layer.size(); ++i ){
        uint8_t& at = target_layer.height(i);

        // fill up to minimum height
        if( at < start_height){
            at = start_height;
            continue;
        }

        const uint8_t level_count = (at - start_height) / level_height;
        at = start_height + level_count * level_height;
    }
}


}   // namespace Effect

