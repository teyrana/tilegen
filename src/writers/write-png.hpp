//Copyright (c) 2022, MIT License, Daniel Williams

#pragma once

#include <cstdio>
#include <string>


namespace Writers {

bool write_data( uint8_t* data, size_t dimx, size_t dimy, const char* fname );

template < typename layer_t> 
bool write_png_heights( layer_t src, const std::string& dest ){
    fprintf( stderr,  ">>> Writing layer: %lu bytes.\n", src.size() );
    return Writers::write_data( src.data(), src.size_x(), src.size_y(), dest.c_str() );
}


}   // namespace Writers
