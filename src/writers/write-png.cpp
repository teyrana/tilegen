//Copyright (c) 2022, MIT License, Daniel Williams
#include <cstdio>
#include <string>

#include <png.h>

// // and also (as of libpng-1.5.0) the zlib header file, if you need it:
// ... DO I need it!?
// #include <zlib.h>

#include "write-png.hpp"


bool Writers::write_data( uint8_t* data, size_t dimx, size_t dimy, const char* fname ){
    fprintf( stderr, ">>> Writing PNG data...\n" );

    FILE *fp = fopen(fname, "wb");
    if (!fp)
       return false;

    // configure png output
    png_structp png_ptr = png_create_write_struct(
                            PNG_LIBPNG_VER_STRING,
                            nullptr, nullptr, nullptr); // this signals default error-handling
    if (!png_ptr)
       return false;

    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
       png_destroy_write_struct(&png_ptr, (png_infopp)nullptr);
       return false;
    }

    png_init_io( png_ptr, fp );

    png_set_IHDR( png_ptr, info_ptr,
                    dimx, dimy,
                    8, PNG_COLOR_TYPE_GRAY, // 8-bit grayscale == 1 byte per pixel
                    PNG_INTERLACE_NONE,
                    PNG_COMPRESSION_TYPE_DEFAULT,
                    PNG_FILTER_TYPE_DEFAULT );

    png_write_info( png_ptr, info_ptr );

    {
        for( size_t yj = dimy; 0 < yj; --yj ){
            const uint8_t* row_ptr = static_cast<uint8_t*>(data + (yj-1)*dimx);
            png_write_row( png_ptr, row_ptr );
        }
    }

    png_write_end( png_ptr, nullptr );

    // success: cleanup
    fclose(fp);
    png_destroy_write_struct( &png_ptr, &info_ptr );

    return true;
}
