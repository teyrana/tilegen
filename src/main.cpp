//Copyright (c) 2022, MIT License, Daniel Williams


    // enum topology_t {TOPOLOGY_ASTEROID, TOPOLOGY_CLIFF, TOPOLOGY_DELTA, 
    //     TOPOLOGY_HILLY, TOPOLOGY_ISLANDS, TOPOLOGY_MOUNTAIN_VALLEY, TOPOLOGY_PLAINS };
    
    // static const char* get_topology_t_names(topology_t topo_type);
    
    // enum geology_t {GEOLOGY_DIRT, GEOLOGY_DIRT_BEDROCK, GEOLOGY_KARST, 
    //             GEOLOGY_ROCK, GEOLOGY_VOLCANO};
    
    // static const char* get_geology_t_names(geology_t geo_type);

#include <cstdio>
#include <iostream>

#include <cxxopts.hpp>

#include "terrain/map.hpp"
#include "effects/cap-effect.hpp"
#include "effects/fill-effect.hpp"
#include "effects/slope-effect.hpp"
#include "effects/step-effect.hpp"
// #include "effects/simplex_effect.hpp"
// #include "effects/bombardment_effect.hpp"

#include "writers/write-png.hpp"

// global
int verbosity = 0;


int main(int argc, char *argv[]) {
    cxxopts::Options options("TileGen", "Procedural Map Generator");
    options.add_options()
        ("c,config", "Config file path", cxxopts::value<std::string>())
        ("o,output", "Output file path", cxxopts::value<std::string>())
        ("v,verbose", "Increase verbosity of output", cxxopts::value<int>())
        ;
    const auto parsed = options.parse(argc, argv);

    verbosity = parsed.count("verbose");

    // ??::json config; // NYI // TODO

    if( 0 < parsed.count("config") ){
        const std::string inpath = parsed["config"].as<std::string>();
        if( ! inpath.empty() ){
            // pass // NYI
        }
    }

    if( 0 < parsed.count("output") ){
        const std::string outpath = parsed["output"].as<std::string>();
        if( ! outpath.empty() ){
            // pass // NYI
        }
    }


    Terrain::Map<32, 32> map( 10.0 );

    const bool add_bedrock = false;
    const bool add_soil = true;

    if( add_bedrock ){   // bedrock layer:
        auto& bedrock_layer = map.get( map.emplace(Terrain::MATERIAL_ROCK_BEDROCK) );
        printf("+> emplacing Bedrock layer: \n" );

        Effects::apply_fill_effect( 12, bedrock_layer );
    }

    if( add_soil ){   // dirt
        auto& soil_layer = map.get( map.emplace(Terrain::MATERIAL_SOIL) );
        printf("+> emplacing soil layer: \n");

        Effects::apply_fill_effect( 120, soil_layer );

        // NOTE: the units on this are still wrong
        Effects::apply_slope_effect( 4.8f, -2.0f, soil_layer );

        Effects::apply_step_effect( 96, 20, soil_layer );

    }

    printf( "<< terrain finished.\n");

    // const bool generate_simplex_hills = false; //false;
    // const bool apply_impacts = false;

	// if (generate_simplex_hills){
	//     gen->load_grid( 200, 200, 250);
	//     SimplexEffect simplex;
    
	//     simplex.set_persistence( 0.8);
	//     simplex.set_frequency( 0.95/gen->scale);
	//     simplex.apply_effect( 4, 0, 50);
	// }

	// if (apply_impacts){
	//     //((Terrain*)gen)->generate_topology_cliff( 23);

	//     BombardmentEffect bomb;
	//     bomb.setMesh( gen);
	//     fprintf(stderr, "    set effect mesh.\n");

	//     // ApplyMeteorImpacts
	//     //bomb.apply_effect( 1, 10, 1000);
	//     //bomb.apply_effect( 1, 0.1, 0.25);
	//     bomb.apply_effect( 1000, 0.9, 23);
	//     bomb.apply_effect( 540, 0.1, 12);
	//     fprintf(stderr, "    Effect applied.\n");
	// }


    // DEBUG // Verification
    if( map ){
        // fprintf( stderr, "    >> Dumping Zeroth layer: \n");
        // map.get_layer_by_index(0).print();

        const std::string png_out_path("debug.heights.png");
        fprintf( stderr, "    >> Dumping Soil Layer (as: PNG)(to: %s)\n", png_out_path.c_str() );
        Writers::write_png_heights( map.get_layer_by_index(0), png_out_path );

        fprintf( stderr, "    >>> Dumpping First layer: \n");
        map.get_layer_by_index(0).print();
    }

    // fprintf( stderr, ">>> Dump first layer: \n");
    //gen->write_to_header( "grid" );
    // gen->write_to_obj_vf( mesh_filename);
    // fprintf(stderr, "    File written.\n");

    return 0;
}
