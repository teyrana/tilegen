//Copyright (c) 2022, MIT License, Daniel Williams
#pragma once

#include <memory>
#include <stack>

#include "layer.hpp"

namespace Terrain {

template< size_t size_x_, size_t size_y_>
class Map {
public:
    using layer_t = Layer<size_x_,size_y_>;

public: 
    Map( float stride = 10.0f )
        : cell_width_m_(stride)
    {}

    ~Map() = default;

    // enum topology_t {TOPOLOGY_ASTEROID, TOPOLOGY_CLIFF, TOPOLOGY_DELTA, 
    //     TOPOLOGY_HILLY, TOPOLOGY_ISLANDS, TOPOLOGY_MOUNTAIN_VALLEY, TOPOLOGY_PLAINS };
    // static const char* get_topology_t_names(topology_t topo_type);
    // enum geology_t {GEOLOGY_DIRT, GEOLOGY_DIRT_BEDROCK, GEOLOGY_KARST, 
    //             GEOLOGY_ROCK, GEOLOGY_VOLCANO};
    // static const char* get_geology_t_names(geology_t geo_type);


// ====== ====== Accessors ====== ====== 
public:
    float cell_width() const { return cell_width_m_; }
    float scale_factor() const { return cell_width_m_; }

    /// \brief number of cells across the grid, x-direction 
    size_t size_x() const { return size_x_; }

    /// \brief number of cells across the grid, y-direction 
    size_t size_y() const { return size_y_; }

    /// \brief number of contained map layers
    size_t count() const { return layers.size(); }

    layer_t& get( size_t index ){ return *layers[index]; }
    layer_t& get_layer_by_index( size_t index ){ return *layers[index]; }

    explicit operator bool() const { return 0 < layers.size(); }

// ====== ====== Generation Functions ====== ====== 
public:

    size_t emplace( material_t mat ){
        const size_t index = layers.size();
        layers.emplace_back( std::make_unique<layer_t>(mat) );
        return index;
    }


// ====== ====== External Functions ====== ====== 
private:

    std::vector< std::unique_ptr<layer_t> > layers;

    float cell_width_m_;  // aka: scale-factor

};

template class Map<16,16>;
template class Map<32,32>;
template class Map<64,64>;
template class Map<1024,1024>;

} // namespace Terrain
