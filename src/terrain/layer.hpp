//Copyright (c) 2022, MIT License, Daniel Williams
#pragma once

#include <cstring>
#include <array>

#include "material.hpp"

namespace Terrain { 

template< size_t size_x_, size_t size_y_ >
class Layer {
public:
    Layer( material_t _material = MATERIAL_SOIL )
        :material(_material)
    {}

    ~Layer() = default;

    uint8_t* data() {
            return heights.data();
    }

    uint8_t& height( size_t xi, size_t yj ) {
            return heights[ index(xi,yj) ];
    }

    uint8_t& height( size_t i ) {
            return heights[i];
    }

    /// \brief number of cells across the grid, x-direction 
    size_t size() const { return heights.size(); }

    /// \brief number of cells across the grid, x-direction 
    size_t size_x() const { return size_x_; }

    /// \brief number of cells across the grid, y-direction 
    size_t size_y() const { return size_y_; }

    static size_t index( size_t i, size_t j ) { return i + j * size_x_; }

    void print() {
        const char indent[] = "        ";
        const size_t spacer_interval = 8;
        printf( "%s>>> Dumping Map (to: stdout)(as: ASCII):", indent );

        for( size_t yj = size_y_; 0 < yj; --yj ){
            if( 0 == (yj%spacer_interval))
                putchar('\n');

            printf("\n%s  ", indent );
            
            for( size_t xi = 0; xi < size_x_; ++xi ){
                if( 0 == (xi%spacer_interval) )
                    putchar(' ');

                printf( "%02X ", height(xi,yj-1) );
            }
        }
        printf("\n\n");
        return;
    }

// ====== ====== External Functions ====== ====== 
public:
    const material_t material;

private:
    std::array<uint8_t, (size_x_*size_y_) > heights;

};

} // namespace Terrain 
