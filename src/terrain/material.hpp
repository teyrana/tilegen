//Copyright (c) 2022, MIT License, Daniel Williams
#pragma once

namespace Terrain { 


enum material_t {
    // generally the bottom layer of the map -- impenetrable, for simplicity
    MATERIAL_ROCK              = 0x1000,
    MATERIAL_ROCK_BEDROCK      = 0x1001,
    MATERIAL_ROCK_ORE          = 0x1010,
    MATERIAL_ROCK_ORE_BAUXITE  = 0x1010,

    // tought, but human-created (also human-destroyed)
    MATERIAL_CONCRETE          = 0x1100,

    // unstable material, will not hold a shape, only a heap
    // easily errodable
    MATERIAL_SAND              = 0x1200,

    // necessary for vegetation to grow
    MATERIAL_SOIL              = 0x1400,

    MATERIAL_WATER             = 0x1600,
    MATERIAL_WATER_FRESH       = 0x1601, // represents fresh water -- will form rivers/lakes and flow across the terrain
    MATERIAL_WATER_SALT        = 0x1602, // represents salt water and seas

};

}   // namespace Terrain