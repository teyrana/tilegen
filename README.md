Procedural Map Generation Tool
-------

This code is a hobby project to experiment with map generation.

This tool is so far experimental, and is not (yet) compatible with anything.


Output Targets
====
Terrain is generated from scratch, and may be output in:
1. Bitmap
2. ??? 
3. Tiled JSON format (NYI)




Implemented Terrain Effects:
====
Base terrain starts as a simple flat plain, expressed as an ortholinear grid.  Effects are apply in a specified order, as configured.

...No Effects are yet implemented...


Planned Effects:
====
1. simplex noise effect
    a. configurable: wrapping and non-wrapping
2. slant effect 
3. White Noise effect
4. erosion (?)
5. Craters (Bombardment)
7. Rivers
6. Roads
8. Towns + Roads



License
-------
Project-Internal Code is MIT-Licensed

### External Libraries:
1. 'cxxopts': (C) Jarryd Beck - MIT License
2. 'simplexnoise': (C) Eliot Eshelman - GPLv3


Planned integrations:
====
https://github.com/SSBMTonberry/tileson
